CREATE DATABASE  IF NOT EXISTS `ridepaldb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ridepaldb`;
-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: ridepaldb
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `dz_album_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `cover_url` varchar(255) NOT NULL,
  `tracklist_url` varchar(255) NOT NULL,
  `dz_genre_id` int(11) NOT NULL,
  `dz_artist_id` int(11) NOT NULL,
  PRIMARY KEY (`dz_album_id`),
  KEY `fk_albums_artists1_idx` (`dz_artist_id`),
  KEY `fk_albums_genres1_idx` (`dz_genre_id`),
  CONSTRAINT `fk_albums_artists1` FOREIGN KEY (`dz_artist_id`) REFERENCES `artists` (`dz_artist_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_albums_genres1` FOREIGN KEY (`dz_genre_id`) REFERENCES `genres` (`dz_genre_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `artists`
--

DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists` (
  `dz_artist_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `picture_url` varchar(255) NOT NULL,
  `tracklist_url` varchar(255) NOT NULL,
  `dz_genre_id` int(11) NOT NULL,
  PRIMARY KEY (`dz_artist_id`),
  KEY `fk_artists_genres1_idx` (`dz_genre_id`),
  CONSTRAINT `fk_artists_genres1` FOREIGN KEY (`dz_genre_id`) REFERENCES `genres` (`dz_genre_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres` (
  `dz_genre_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `picture_url` varchar(255) NOT NULL,
  PRIMARY KEY (`dz_genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `playlist_genres_junction`
--

DROP TABLE IF EXISTS `playlist_genres_junction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist_genres_junction` (
  `playlist_id` int(11) NOT NULL,
  `dz_genre_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT 100,
  PRIMARY KEY (`playlist_id`,`dz_genre_id`),
  KEY `fk_playlist_genres_junction_genres_idx` (`dz_genre_id`),
  CONSTRAINT `fk_playlist_genres_junction_genres` FOREIGN KEY (`dz_genre_id`) REFERENCES `genres` (`dz_genre_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlist_genres_junction_playlists` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`playlist_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `playlist_tracks_junction`
--

DROP TABLE IF EXISTS `playlist_tracks_junction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist_tracks_junction` (
  `playlist_id` int(11) NOT NULL,
  `dz_track_id` int(11) NOT NULL,
  PRIMARY KEY (`playlist_id`,`dz_track_id`),
  KEY `fk_playlist_tracks_junction_tracks1_idx` (`dz_track_id`),
  CONSTRAINT `fk_playlist_tracks_junction_playlists` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`playlist_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlist_tracks_junction_tracks1` FOREIGN KEY (`dz_track_id`) REFERENCES `tracks` (`dz_track_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `playlists`
--

DROP TABLE IF EXISTS `playlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists` (
  `playlist_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `is_hidden` tinyint(3) NOT NULL DEFAULT 0,
  `duration` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_on` date NOT NULL DEFAULT current_timestamp(),
  `modified_on` date DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `picture_url` varchar(512) NOT NULL DEFAULT 'playlistImage.png',
  PRIMARY KEY (`playlist_id`),
  KEY `fk_playlists_users1_idx` (`user_id`),
  CONSTRAINT `fk_playlists_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `token_id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tracks`
--

DROP TABLE IF EXISTS `tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracks` (
  `dz_track_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `duration` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `explicit_lyrics` tinyint(4) NOT NULL,
  `link` varchar(255) NOT NULL,
  `preview_url` varchar(255) NOT NULL,
  `dz_genre_id` int(11) NOT NULL,
  `dz_artist_id` int(11) NOT NULL,
  `dz_album_id` int(11) NOT NULL,
  PRIMARY KEY (`dz_track_id`),
  KEY `fk_tracks_genres1_idx` (`dz_genre_id`),
  KEY `fk_tracks_artists1_idx` (`dz_artist_id`),
  KEY `fk_tracks_albums1_idx` (`dz_album_id`),
  CONSTRAINT `fk_tracks_albums1` FOREIGN KEY (`dz_album_id`) REFERENCES `albums` (`dz_album_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tracks_artists1` FOREIGN KEY (`dz_artist_id`) REFERENCES `artists` (`dz_artist_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tracks_genres1` FOREIGN KEY (`dz_genre_id`) REFERENCES `genres` (`dz_genre_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `display_name` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar_url` varchar(255) DEFAULT 'defaultAvatar.png',
  `is_deleted` tinyint(3) NOT NULL DEFAULT 0,
  `role_id` int(11) NOT NULL,
  `created_on` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`user_id`),
  KEY `fk_users_roles1_idx` (`role_id`),
  CONSTRAINT `fk_users_roles1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-07 23:02:35
