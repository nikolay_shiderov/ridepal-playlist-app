import express from 'express';
import { authMiddleware } from '../authentication/auth.middleware.js';
import usersData from '../data/users-data.js';
import bodyValidator from '../middleware/body-validator.js';
import loggedUserGuard from '../middleware/logged-user-guard.js';
import serviceErrors from '../services/service-errors.js';
import usersServices from '../services/users-services.js';
import { createUserSchema } from '../validations/createUserSchema.js';
import { updateUserSchema } from '../validations/updateUserSchema.js';
import processAvatar from '../middleware/process-avatar.js';
import fileValidator from '../middleware/file-validator.js';
import { updateAvatarSchema } from '../validations/updateAvatarSchema.js';
import playlistsServices from '../services/playlists-services.js';
import playlistsData from '../data/playlists-data.js';

const usersController = express.Router();

usersController
  .post('/register', bodyValidator('user', createUserSchema), async (req, res) => {
    const data = req.body;
    const { error, user } = await usersServices.createUser(usersData)(data);

    if (error === serviceErrors.DUPLICATE_RECORD) {
      return res.status(409).json({ error: 'Username or email has already been used!' });
    }
    res.status(201).json({
      username: user.username,
      display_name: user.display_name,
      email: user.email,
      created_on: user.created_on.toLocaleDateString(),
    });
  })
  .post('/login', async (req, res) => {
    try {
      const data = req.body;
      const { error, token } = await usersServices.loginUser(usersData)(data);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(400).json({ error: 'Username does not exist' });
      }

      if (error === serviceErrors.INVALID_PASSWORD) {
        return res.status(400).json({ error: 'Password does not match username!' });
      }

      res.status(200).json({ token });
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  })
  .post('/logout', authMiddleware, loggedUserGuard, async (req, res) => {
    await usersServices.logoutUser(usersData)(req.headers.authorization.replace('Bearer ', ''));

    res.json({ message: 'Successfully logged out!' });
  })
  .get('/:id', authMiddleware, loggedUserGuard, async (req, res) => {
    const { id } = req.params;

    const { error, user } = await usersServices.getUserById(usersData)(+id, req.user);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'User not found!' });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(403).json({ error: 'You are not authorized!' });
    } else {
      res.json(user);
    }
  })
  .get('/:id/playlists', authMiddleware, loggedUserGuard, async (req, res) => {
    const { id: userId } = req.params;
    const { user_id: loggedUserId, role_id: roleId } = req.user;

    const { error, playlists } = await playlistsServices.getAllPlaylistsByUser(playlistsData)(+userId, +roleId, +loggedUserId);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'No playlists found!' });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(403).json({ error: 'You are not authorized!' });
    } else {
      res.json(playlists);
    }
  })
  .put('/:id', authMiddleware, loggedUserGuard, bodyValidator('user', updateUserSchema), async (req, res) => {
    const { id } = req.params;

    const { error, user } = await usersServices.updateUser(usersData)(req.user, req.body, +id);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'User not found' });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(403).json({ error: 'You are not authorized!' });
    } else {
      res.json(user);
    }
  })
  .put('/:id/avatar', authMiddleware, loggedUserGuard, processAvatar, fileValidator('avatar', updateAvatarSchema), async (req, res) => {
    const { id } = req.params;
    const { error, updatedAvatar } = await usersServices.updateUserAvatar(usersData)(req.user, req.file.filename, +id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'User not found' });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(403).json({ error: 'You are not authorized!' });
    } else {
      res.json({ updatedAvatar });
    }
  });

export default usersController;
