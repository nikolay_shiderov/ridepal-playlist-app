import express from 'express';
import { authMiddleware } from '../authentication/auth.middleware.js';
import mapsData from '../data/maps-data.js';
import loggedUserGuard from '../middleware/logged-user-guard.js';
import mapsServices from '../services/maps-services.js';
import { getLocationSchema } from '../validations/getLocationSchema.js';
import bodyValidator from '../middleware/body-validator.js';
import { getTravelInformationSchema } from '../validations/getTravelInformationSchema.js';
import { getRouteSchema } from '../validations/getRouteSchema.js';
import serviceErrors from '../services/service-errors.js';

const mapsController = express.Router();

mapsController.use(authMiddleware);
mapsController.use(loggedUserGuard);

mapsController
  .get('/locations', bodyValidator('locations', getLocationSchema), async (req, res) => {
    const { origin, destination } = req.body;
    const response = await mapsServices.getLocations(mapsData)(origin, destination);

    if (response.error) {
      return res.status(404).json({
        error: `${Object.keys(response)
          .filter((key) => response[key] === null)
          .join(' and ')} not found!`,
        origins: response.origins,
        destinations: response.destinations,
      });
    } else {
      res.json({ origins: response.origins, destinations: response.destinations });
    }
  })
  .get('/travel-information', bodyValidator('travel-information', getTravelInformationSchema), async (req, res) => {
    const { originPoint, destinationPoint } = req.body;
    const response = await mapsServices.getDistanceMatrix(mapsData)(originPoint, destinationPoint);

    if (response.error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ message: `No driving options between point(${originPoint}) and point(${destinationPoint})` });
    } else {
      res.json(response);
    }
  })
  .post('/route', bodyValidator('route', getRouteSchema), async (req, res) => {
    const { origin, destination } = req.body;
    const response = await mapsServices.getRoute(mapsData)(origin, destination);

    if (response.error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ message: `No driving options between ${origin.toUpperCase()} and ${destination.toUpperCase()}` });
    } else {
      res.json(response);
    }
  });

export default mapsController;
