import multer from 'multer';
import path from 'path';

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads/avatars');
  },
  filename(req, file, cb) {
    const filename = `uid${req.user.user_id}_${Date.now()}${path.extname(file.originalname)}`;

    cb(null, filename);
  },
});

export default multer({ storage }).single('avatar');
