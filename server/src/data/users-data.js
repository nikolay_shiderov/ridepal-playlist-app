import pool from './pool.js';

const getAll = async () => {
  const sql = `
  SELECT user_id, username, display_name, email, avatar_url, is_deleted, role_id, created_on
  FROM users; 
  `;
  return await pool.query(sql);
};

const getBy = async (column, value) => {
  const sql = `
  SELECT u.user_id, u.username, u.display_name, u.email, u.avatar_url, u.created_on, u.is_deleted, u.role_id, 
  count(p.playlist_id) playlists, IFNULL(sum(p.duration), 0) as play_time
  FROM users u
  JOIN playlists p ON p.user_id = u.user_id 
  WHERE u.${column} = ?
`;

  return (await pool.query(sql, [value]))[0];
};

const create = async (username, email, password, role) => {
  const sql = `
  INSERT INTO users (username, display_name, email, password, role_id)
  values (?, ?, ?, ?, ?)
  `;
  const user = await pool.query(sql, [username, username, email, password, role]);

  return await getBy('user_id', user.insertId);
};

const update = async (data, id) => {
  const sql = `
  UPDATE users 
  SET display_name = ?, email = ?
  WHERE user_id = ?
  `;
  await pool.query(sql, [data.display_name, data.email, id]);

  return await getBy('user_id', id);

};

const remove = async (id) => {
  const sql = `
  UPDATE users SET is_deleted = 1 WHERE user_id = ?
  `;
  await pool.query(sql, [id]);

  return await getBy('user_id', id);

};

const getUserWithPassword = async (column, value) => {
  const sql = `
  SELECT user_id, username, password, role_id
  FROM users WHERE ${column} = ?
`;

  return (await pool.query(sql, [value]))[0];
};

const addToken = async (token) => {
  const sql = `
  INSERT INTO tokens(token)
  VALUES (?)
  `;
  return await pool.query(sql, [token]);
};

const updateAvatar = async (filename, id) => {
  const sql = `
  UPDATE users SET avatar_url = ?
  WHERE user_id = ?
  `;
  await pool.query(sql, [filename, id]);

  return await getBy('user_id', id);
};

export default {
  create,
  getBy,
  getUserWithPassword,
  addToken,
  getAll,
  remove,
  update,
  updateAvatar,
};
