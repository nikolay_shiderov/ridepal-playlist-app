import mariadb from 'mariadb';
import dotenv from 'dotenv';

const config = dotenv.config().parsed;

const pool = mariadb.createPool({
  host: config.DB_HOST,
  port: config.DB_PORT,
  user: config.DB_USER,
  password: config.DB_PASSWORD,
  database: config.DB_NAME,
});

export default pool;