export const createUserSchema = {
  username: value => typeof value === 'string' && value.length > 3,
  password: value => typeof value === 'string' && value.length > 5,
  email: value => typeof value === 'string' && (/^[^\s@,]+@[^\s@,]+\.[^\s@,]+$/).test(value),
};