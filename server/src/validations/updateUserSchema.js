export const updateUserSchema = {
  display_name: value => typeof value === 'string' && value.length > 3,
  email: value => typeof value === 'string' && (/^[^\s@,]+@[^\s@,]+\.[^\s@,]+$/).test(value),
};