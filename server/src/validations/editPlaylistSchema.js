export const editPlaylistSchema = {
  name: (value) => typeof value === 'string' && value.length > 3,
  hidden: (value) => typeof value === 'number',
};
