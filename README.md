# PlayPal Playlist App

Team: **Nikolay Shiderov** and **Nikola Marinov**

## About

PlayPal is a single-page application that enables your users to generate playlists for specific travel duration periods, based on preferred genres.

## Project Information

- Written in: **JavaScript ES2020**
- Requirements:
  - Node.js 14.0+
  - MariaDB Server
  - Workbench
  - VS Code

## Quick Start Guide:

1. Clone the repo to your preferred folder by running the below command in your terminal:

```
git clone https://gitlab.com/nikola-mrnv/ridepal-playlist-app
```

2. Use the provided `playpal-schema-demo-data.sql` file with demo data OR Load the empty database schema in Workbench (make sure MariaDB server is running) 
   1. Workbench > Server > Data Import
   2. Import from Self-Contained File > Select `playpal-schema-demo-data.sql` (or `playpal-schema-for-seed-no-data.sql`)
   3. Start Import
   
Database schema:
![Database schema](./images/db.png)

1. Navigate to the project's `server` directory and open a new terminal window.

2. Run the below command to install dependencies:

```
npm install
```

5. Once the server dependencies have been installed, create a `.env` file in `server` directory and update the details for your MariaDB configuration (username, password, port, etc.), sample settings below:

```js
DB_USER = root;
DB_PASSWORD = root;
DB_HOST = localhost;
DB_PORT = 3306;
DB_NAME = ridepaldb;

PORT = 4000;

SECRET_KEY = sekreten_kliuch;
```

6. ( **You can skip this step if you loaded the schema with demo-data** ) Now we can populate the empty schema from step `2` by running the provided seed script (it may take several minutes, depending on the computer):

```
npm run seed
```

7. After the seed script has finished you can start backend server:

```
npm start
```

8. Now that the server is running, start a new terminal window in the `client` folder.

9. Run the below command to install dependencies:

```
npm install
```

10. Once all dependencies have been installed, start the frontend app

```
npm start
```

The application is now live.

Feel free to make a new user registration or explore the app using the available admin credentials (username: `admin`, password: `password`).

## Screenshots

Public homepage. Users can check out the top playlists and to listen to any of them they can Register or Login
![Public homepage](./images/0.png)

Register page
![Register screen](./images/1.png)

Login page
![Login screen](./images/2.png)

Registered user homepage. Here users have access to not only publicly available playlists, but also their own.
![Registered homepage](./images/3.png)

User profile page. Here users can set their display name, email address and avatar. Furthermore they can edit their own playlists, either rename them or hide them.
![User profile page](./images/4.png)

Create new playlist page 1/3. Here users enter their start and end points and confirm the trip's duration.
![Create new playlist page 1/3](./images/5.png)

Create new playlist page 2/3. Here users select their genres preferences
![Create new playlist page 2/3](./images/6.png)

Create new playlist page 3/3. Here users can see the generated playlist and decide to save it or generate a new one. Users can set playlist name, hidden status and random cover image.
![Create new playlist page 3/3](./images/7.png)

Admin page. Here admin users can edit/delete both users and playlists.
![Admin page](./images/8.png)

## Backend Details

### Public part

1. POST `/v1/users/register` registers a user with username, password and email and returns the user's information

   Request body

   ```js
   {
     "username": string[length > 3],
     "password": string[length > 5],
     "email": string[valid email address],
   }
   ```

1. POST `/v1/users/login` Logs in a registered user. The response is a unique token used for authentication/authorization of other requests

   Request body

   ```js
   {
     "username": string[length > 3],
     "password": string[length > 5],
   }
   ```

1. POST `/v1/users/logout` checks if the user is logged in and logs out the user by blacklisting the token provided.

1. GET `/v1/playlists/` returns an array containing all stored playlists.

1. GET `/v1/playlists/:id` returns an array containing the tracks of a single playlist.

### Admin actions:

#### **Users:**

1. GET `/v1/admin/users` returns a list of all users (incl. deleted)

1. DELETE `/v1/admin/users/:id` marks a user as deleted (is_deleted = 1), returns a message with the user ID

1. GET `/v1/users/:id` returns a user by ID

1. PUT `/v1/users/:id` updates user details and returns updated user object

   Request body

   ```js
   {
     "display_name": string[length > 3],
     "email": string[valid email address],
   }
   ```

#### **Playlists:**

1. PUT `/v1/playlists/:id` edits a playlist's name, returns updated playlist object

   Request body

   ```js
   {
     "name": string[length > 3],
   }
   ```

1. DELETE `/v1/playlists/:id` marks a playlist as deleted (is_deleted = 1), returns a message with the playlist ID

### Logged user actions:

1. GET `/v1/users/:id` returns user info or Unauthorized

1. PUT `/v1/users/:id` updates user details and returns updated user object or Unauthorized

   Request body

   ```js
   {
     "display_name": string[length > 3],
     "email": string[valid email address],
   }
   ```

1. PUT `/v1/users/:id/avatar` updates user avatar and returns updated avatar filename (avatar_url column)

   Request body (form-data)

   ```js
   "avatar": image file, max 1MB [filetype: GIF, JPG, JPEG, PNG, WEBP, BMP]
   ```

1. GET `/v1/playlists/genres` returns an array of all playlists

1. GET `v1/users/:id/playlists` returns an array of all playlists created by the specific user

1. POST `v1/playlists/generator` generates a playlist based on travelDuration and an array of up to two genres. Returns an object with the generated playlist information and tracks

   Request body

   ```js
   {
    "travelDuration": number,
    "genres": array[
        {
            "dz_genre_id": number,
            "average_duration": number,
            "amount": number
        },
        {
            "dz_genre_id": number,
            "average_duration": number,
            "amount": number
        }
    ]
   }
   ```

   Example

   ```js
   {
    "travelDuration": 3600,
    "genres": [
        {
            "dz_genre_id": 132,
            "average_duration": 214,
            "amount": 0.4
        },
        {
            "dz_genre_id": 116,
            "average_duration": 193,
            "amount": 0.6
        }
    ]
   }
   ```

1. POST `v1/playlists/new-playlist` saves a playlist generated from `v1/playlists/generator`.

   Request body

   ```js
      {
       "duration": number,
       "average_rank": number,
       "nb_tracks": number,
       "genres": array[],
       "tracks": array[],
   ```

1. PUT `/v1/playlists/:id` edits a playlist's name, returns updated playlist object

   Request body

   ```js
   {
     "name": string[length > 3],
   }
   ```

1. DELETE `/v1/playlists/:id` marks a playlist as deleted (is_deleted = 1), returns a message with the playlist ID

1. POST `/v1/maps/route` returns an object with suggested Bing Maps route based on the input values

   Request body

   ```js
   {
       "origin": string[length > 3],
       "destination": string[length > 3]
   }
   ```
