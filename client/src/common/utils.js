const formatDuration = (value) => {
  return new Date(value * 1000).toISOString().substr(11, 8);
};

const formatTravelDuration = (value) => {
  const hours = Math.floor(value / 3600);
  const minutes = Math.round(Math.floor(value % 3600) / 60);
  return hours ? (minutes ? `${hours}h ${minutes}m` : `${hours}h`) : `${minutes}m`;
};

export const utils = {
  formatDuration,
  formatTravelDuration,
};
