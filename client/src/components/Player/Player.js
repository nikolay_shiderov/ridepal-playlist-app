import React, { makeStyles, Paper } from '@material-ui/core';
import { useContext, useEffect, useState } from 'react';
import AuthContext from '../../providers/authContext';
import PlayerControls from '../PlayerControls/PlayerControls';
import TrackList from '../TrackList/TrackList';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 1000,
  },
});

const Player = ({ playlist }) => {
  const tracks = playlist.tracks.map((track, index) => ({ ...track, index: index }));
  const classes = useStyles();
  const [currentTrack, setCurrentTrack] = useState(tracks[0]);
  const [playing, setPlaying] = useState(false);
  const [audio, setAudio] = useState(null);
  const auth = useContext(AuthContext);


  useEffect(() => {
    setAudio(document.getElementsByClassName(`audio-${tracks[0].dz_track_id}`)[0]);
  }, []);

  useEffect(() => {
    if (playing) {
      audio.play();
    }
  }, [audio]);

  const togglePlay = () => {
    if (audio.paused) {
      audio.play();
    } else {
      audio.pause();
    }
    setPlaying(!playing);
  };

  const playTrack = (id) => {
    const track = tracks.find(track => track.dz_track_id === id);
    audio.pause();
    audio.currentTime = 0;
    setCurrentTrack(track);
    setAudio(document.getElementsByClassName(`audio-${id}`)[0]);
  };

  const handleSkip = (track) => {
    audio.pause();
    audio.currentTime = 0;
    setCurrentTrack(track);
    setAudio(document.getElementsByClassName(`audio-${track.dz_track_id}`)[0]);
  };

  return (
    <>
      <Paper className={classes.root}>
        {auth.user &&
          <PlayerControls
            currentTrack={currentTrack}
            togglePlay={togglePlay}
            tracks={tracks}
            playing={playing}
            setCurrentTrack={setCurrentTrack}
            setAudio={setAudio}
            handleSkip={handleSkip}
          />
        }
        <TrackList
          playlist={playlist}
          currentTrack={currentTrack}
          playTrack={playTrack}
        />
      </Paper>
      {tracks
        .map(track =>
          <div key={track.index}>
            <audio className={`audio-${track.dz_track_id}`}>
              <source src={track.preview_url} />
            </audio>
          </div>)
      }
    </>
  );
};

export default Player;
