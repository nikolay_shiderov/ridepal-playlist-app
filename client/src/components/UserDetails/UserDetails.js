import {
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
  Typography,
} from '@material-ui/core';
import React, { useState } from 'react';
import PersonIcon from '@material-ui/icons/Person';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import TimelineIcon from '@material-ui/icons/Timeline';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import { utils } from '../../common/utils';

const UserDetails = ({ userData, formik }) => {

  const [editEmail, setEditEmail] = useState(false);
  const [editName, setEditName] = useState(false);

  const handleChange = (e) => {
    formik.handleChange(e);
    formik.handleBlur(e);
  };

  const handleSave = (e) => {
    setEditName(false);
    setEditEmail(false);
    formik.handleSubmit(e);
  };

  return (
    <Grid style={{ display: 'flex', flexDirection: 'column', marginLeft: '24px' }}>
      {editName
        ? <Grid container style={{ marginBottom: '16px' }}>
          <TextField
            id="display_name"
            name="display_name"
            label="Display name"
            style={{ flex: 1 }}
            value={formik.values.display_name}
            error={formik.touched.display_name && Boolean(formik.errors.display_name)}
            helperText={formik.touched.display_name && formik.errors.display_name}
            onChange={handleChange}
          />
          <IconButton
            onClick={handleSave}>
            <SaveIcon />
          </IconButton>
        </Grid>
        : <>
          <Typography component="h1" variant="h4" gutterBottom>
            {formik.values.display_name}
            <IconButton
              onClick={() => setEditName(true)}>
              <EditIcon />
            </IconButton>
          </Typography>
        </>
      }
      <Grid container spacing={5} >
        <Grid item>
          <Typography variant="h5" align="center"  >
            {userData.playlists}
          </Typography>
          <Typography
            align="center"
            gutterBottom>
            PLAYLISTS
                    </Typography>
        </Grid>
        <Grid item>
          <Typography variant="h5" align="center"  >
            {utils.formatDuration(userData.play_time)}
          </Typography>
          <Typography align="center" gutterBottom>
            TOTAL PLAY TIME
                    </Typography>
        </Grid>
      </Grid>
      <List>
        <ListItem>
          <ListItemAvatar>
            <PersonIcon style={{ fontSize: '2.5rem' }} />
          </ListItemAvatar>
          <ListItemText
            primary="Username"
            secondary={userData.username}
          />
        </ListItem>
        <ListItem>
          <ListItemAvatar>
            <AlternateEmailIcon style={{ fontSize: '2.5rem' }} />
          </ListItemAvatar>
          {editEmail
            ? <Grid container style={{ marginBottom: '16px' }}>
              <TextField
                id="email"
                name="email"
                label="Email address"
                style={{ flex: 1 }}
                value={formik.values.email}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
                onChange={handleChange}
              />
              <IconButton
                onClick={handleSave}>
                <SaveIcon />
              </IconButton>
            </Grid>
            : <>
              <ListItemText
                style={{ flex: 'initial' }}
                primary="Email address"
                secondary={formik.values.email}
              />
              <IconButton
                onClick={() => setEditEmail(true)}
                style={{ marginLeft: '16px' }} >
                <EditIcon />
              </IconButton>
            </>
          }
        </ListItem>
        <ListItem>
          <ListItemAvatar>
            <TimelineIcon style={{ fontSize: '2.5rem' }} />
          </ListItemAvatar>
          <ListItemText
            primary="Member since"
            secondary={new Date(userData.created_on).toLocaleDateString()}
          />
        </ListItem>
      </List>
    </Grid>
  );
};

export default UserDetails;
