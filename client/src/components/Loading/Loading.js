import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  loading: {
    maxWidth: '50%',
    margin: 'auto',
    paddingTop: '5%',
    paddingBottom: '5%',
  },
});

const Loading = () => {

  const classes = useStyles();

  return <div className={classes.loading}><LinearProgress /></div>;
};

export default Loading;
