import React, { useState } from 'react';
import { Stepper, Step, StepLabel, Button, Typography, Box, makeStyles, Container } from '@material-ui/core';
import TravelInfo from '../TravelInfo/TravelInfo.js';
import GenrePreference from '../GenrePreference/GenrePreference.js';
import SaveNewPlaylist from '../SaveNewPlaylist/SaveNewPlaylist.js';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  centered: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
}));

function getSteps() {
  return ['Travel Information', 'Genre Preferences', 'Confirm & Save'];
}

const Generator = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();
  const [routeInfo, setRouteInfo] = useState(null);
  const [locations, setLocations] = useState({
    origin: '',
    destination: '',
  });
  const [locationsError, setLocationsError] = useState(null);
  const [checkboxes, setCheckboxes] = useState([]);
  const [genres, setGenres] = useState([]);
  const [allowUniqueArtists, setAllowUniqueArtists] = useState(false);
  const [hidden, setHidden] = useState(false);
  const [sliderValue, setSliderValue] = useState(50);
  const [payload, setPayload] = useState({
    travelDuration: 0,
    genres: [],
  });
  const [disabledNext, setDisabledNext] = useState(true);

  function getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <TravelInfo
            routeInfo={routeInfo}
            setRouteInfo={setRouteInfo}
            locations={locations}
            setLocations={setLocations}
            locationsError={locationsError}
            setLocationsError={setLocationsError}
            setDisabledNext={setDisabledNext}
          />
        );
      case 1:
        return (
          <GenrePreference
            checkboxes={checkboxes}
            setCheckboxes={setCheckboxes}
            sliderValue={sliderValue}
            setSliderValue={setSliderValue}
            setGenres={setGenres}
            genres={genres}
            payload={payload}
            setPayload={setPayload}
            routeInfo={routeInfo}
            setDisabledNext={setDisabledNext}
            allowUniqueArtists={allowUniqueArtists}
            setAllowUniqueArtists={setAllowUniqueArtists}
          />
        );
      case 2:
        return <SaveNewPlaylist payload={payload} locations={locations} hidden={hidden} setHidden={setHidden} />;
      default:
        return 'Unknown stepIndex';
    }
  }

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <Container maxWidth="md">
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        <Box display="flex" flexDirection="row" justifyContent="center">
          {activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>All steps completed</Typography>
              <Button onClick={handleReset}>Reset</Button>
            </div>
          ) : (
            <div>
              <div className={classes.instructions}>{getStepContent(activeStep)}</div>
              <div className={classes.centered}>
                <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>
                  Back
                </Button>
                {activeStep !== steps.length - 1 && (
                  <Button variant="contained" color="primary" onClick={handleNext} disabled={disabledNext}>
                    Next
                  </Button>
                )}
              </div>
            </div>
          )}
        </Box>
      </div>
    </Container>
  );
};

export default Generator;
