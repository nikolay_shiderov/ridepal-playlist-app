import { Button, Grid, Link, makeStyles, Typography } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import React from 'react';
import { useHistory } from 'react-router';

const useStyles = makeStyles({
  image: {
    backgroundImage: 'url(https://source.unsplash.com/featured/?music)',
    backgroundRepeat: 'no-repeat',
    backgroundColor: grey[50],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  registerButton: {
    position: 'fixed',
    bottom: '40px',
    marginLeft: '30px',
  },
  title: {
    color: 'white',
    backgroundColor: grey[900],
    padding: '20px',
    marginLeft: '30px',
    marginTop: '30px',
    borderRadius: 4,
  },
});

const HomeImage = () => {

  const classes = useStyles();
  const history = useHistory();

  return (
    <Grid
      item
      container
      xs={12}
      md={4}
      className={classes.image}>
      <Grid>
        <Typography
          gutterBottom
          component="h1"
          variant="h3"
          className={classes.title}>
          Welcome to<br />
               PlayPal
          </Typography>
      </Grid>
      <Grid className={classes.registerButton}>
        <Grid>
          <Button
            color='secondary'
            variant='contained'
            onClick={() => history.push('/register')}
          >
            Register now
            </Button>
        </Grid>
        <Grid>
          <Link href="/login" variant="body2" style={{ color: 'antiquewhite' }}>
            Already have an account? Log In
            </Link>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default HomeImage;