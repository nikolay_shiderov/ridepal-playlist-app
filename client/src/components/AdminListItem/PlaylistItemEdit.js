import { Button, Checkbox, FormControlLabel, Grid, TextField } from '@material-ui/core';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { editPlaylistValidationSchema } from '../../validations/edit-playlist-validation-schema.js';
import React from 'react';
import { useState } from 'react';
import { playlistRequests } from '../../services/playlist-requests.js';
import UserMessage from '../UserMessage/UserMessage.js';

const PlaylistItemEdit = (props) => {
  const { name, playlist_id, is_deleted, is_hidden, playlists, setPlaylists, setExpanded } = props;
  const [hidden, setHidden] = useState(is_hidden);
  const schema = editPlaylistValidationSchema(yup);
  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: '',
  });

  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const formik = useFormik({
    initialValues: {
      name: name,
    },
    validationSchema: schema,
    onSubmit: playlistRequests.editPlaylist(playlist_id, hidden, playlists, setPlaylists, setExpanded, setUserMessage),
  });

  const handleChange = (e) => setHidden(Number(e.target.checked));

  return (
    <>
      <form>
        <Grid container spacing={3} alignItems="center">
          <Grid item xs={9}>
            <TextField
              fullWidth
              margin="normal"
              id="name"
              name="name"
              label="Playlist name"
              value={formik.values.name}
              onChange={formik.handleChange}
              error={formik.touched.name && Boolean(formik.errors.name)}
              helperText={formik.touched.name && formik.errors.name}
              disabled={is_deleted === 1}
            />
          </Grid>
          <Grid item xs={1} align="center">
            <FormControlLabel
              control={<Checkbox checked={Boolean(hidden)} onChange={handleChange} name="hidden" disabled={is_deleted === 1} />}
              label="Hidden"
            />
          </Grid>
          <Grid item xs={2} align="center">
            <Button color="primary" variant="contained" disabled={is_deleted === 1} onClick={formik.handleSubmit}>
              Save and Close
          </Button>
          </Grid>
        </Grid>
      </form>
      <UserMessage
        open={userMessage.open}
        handleClose={userMsgClose}
        severity={userMessage.severity}
        message={<span>{userMessage.message}</span>} />
    </>
  );
};

export default PlaylistItemEdit;
