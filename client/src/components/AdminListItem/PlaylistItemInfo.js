import { Grid, Typography } from '@material-ui/core';
import React from 'react';

const PlaylistItemInfo = ({ playlist_id, name, user_id, duration, is_deleted }) => {
  return (
    <Grid>
      <Typography>{`ID: ${playlist_id} / NAME: ${name} / BY: ${user_id} / DURATION: ${duration} / DELETED: ${is_deleted ? 'YES' : 'NO'}`}</Typography>
    </Grid>
  );
};

export default PlaylistItemInfo;
