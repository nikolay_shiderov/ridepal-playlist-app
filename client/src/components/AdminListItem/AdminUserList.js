import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import AdminListItem from './AdminListItem.js';

const AdminUserList = ({ users, setUsers }) => {
  return (
    <Grid container spacing={2}>
      {users.length > 0 ? (
        users.map((user) => (
          <Grid item xs={12} key={user.user_id}>
            <AdminListItem {...user} users={users} setUsers={setUsers} itemInfo="user" />
          </Grid>
        ))
      ) : (
        <Typography>There are no users to show</Typography>
      )}
    </Grid>
  );
};

export default AdminUserList;
