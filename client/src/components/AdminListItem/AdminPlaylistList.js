import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import AdminListItem from './AdminListItem.js';

const AdminPlaylistList = ({ playlists, setPlaylists }) => {
  return (
    <Grid container spacing={2}>
      {playlists.length > 0 ? (
        playlists.map((playlist) => (
          <Grid item xs={12} key={playlist.playlist_id}>
            <AdminListItem {...playlist} playlists={playlists} setPlaylists={setPlaylists} itemInfo="playlist" />
          </Grid>
        ))
      ) : (
        <Typography>There are no playlists to show</Typography>
      )}
    </Grid>
  );
};

export default AdminPlaylistList;
