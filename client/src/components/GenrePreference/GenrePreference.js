import React, { useEffect } from 'react';
import {
  Checkbox,
  Container,
  Divider,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Grid,
  makeStyles,
  Slider,
  Typography,
} from '@material-ui/core';
import { playlistRequests } from '../../services/playlist-requests.js';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing(2),
  },
  formLabel: {
    marginBottom: theme.spacing(2),
  },
  genrePresence: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(2),
  },
}));

const GenrePreference = ({
  checkboxes,
  setCheckboxes,
  sliderValue,
  setSliderValue,
  setGenres,
  payload,
  setPayload,
  routeInfo,
  genres,
  setDisabledNext,
  allowUniqueArtists,
  setAllowUniqueArtists,
}) => {
  const classes = useStyles();
  const checkedCheckboxes = checkboxes.filter((checkbox) => checkbox.checked);

  useEffect(() => {
    playlistRequests.getGenreCheckboxes(setCheckboxes, setDisabledNext);
  }, []);

  useEffect(() => {
    if (genres.length === 2) {
      const payloadGenres = genres.map((genre) => ({ dz_genre_id: genre.dz_genre_id, amount: 0 }));
      payloadGenres[0].amount = sliderValue;
      payloadGenres[1].amount = 100 - sliderValue;
      setPayload({ ...payload, genres: payloadGenres, travelDuration: routeInfo.travelDuration, uniqueArtists: allowUniqueArtists });
      setDisabledNext(false);
    } else if (genres.length === 1) {
      const payloadGenre = genres.map((genre) => ({ dz_genre_id: genre.dz_genre_id, amount: 100 }));
      setPayload({ ...payload, genres: payloadGenre, travelDuration: routeInfo.travelDuration, uniqueArtists: allowUniqueArtists });
      setDisabledNext(false);
    }
  }, [genres.length, sliderValue, allowUniqueArtists]);

  useEffect(() => {
    if (checkedCheckboxes.length === 2) {
      const updatedCheckboxes = checkboxes.map((checkbox) => (checkbox.checked ? checkbox : { ...checkbox, disabled: true }));
      setCheckboxes(updatedCheckboxes);
      setDisabledNext(true);
    } else {
      const updatedCheckboxes = checkboxes.map((checkbox) => ({ ...checkbox, disabled: false }));
      setCheckboxes(updatedCheckboxes);
      setDisabledNext(true);
    }
    setGenres(checkedCheckboxes);
  }, [checkedCheckboxes.length]);

  const handleChange = (e) => {
    const updatedCheckboxes = checkboxes.map((checkbox) =>
      // eslint-disable-next-line comma-dangle
      checkbox.name === e.target.name || checkbox.checked ? { ...checkbox, checked: e.target.checked } : checkbox
    );
    setCheckboxes(updatedCheckboxes);
  };

  const handleSliderChange = (event, value) => setSliderValue(value);

  const handleUniqueArtistsChange = () => {
    setAllowUniqueArtists(!allowUniqueArtists);
  };

  return (
    <Container maxWidth="sm">
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend" align="center" className={classes.formLabel}>
          <Typography variant="h4">Select up to 2 genres:</Typography>
        </FormLabel>
        <FormGroup>
          <Grid container spacing={2}>
            {checkboxes &&
              checkboxes.map((checkbox) => (
                <Grid key={checkbox.dz_genre_id} item xs={6} sm={4}>
                  <FormControlLabel
                    control={<Checkbox checked={checkbox.checked} onChange={handleChange} name={checkbox.name} disabled={checkbox.disabled} />}
                    label={checkbox.name}
                  />
                </Grid>
              ))}
          </Grid>
          <Divider />
          <FormControlLabel
            control={<Checkbox checked={allowUniqueArtists} onChange={handleUniqueArtistsChange} name="allow-unique-artists" />}
            label="Unique artists only"
          />
        </FormGroup>
        {checkedCheckboxes.length === 2 && (
          <div>
            <Typography variant="h4" align="center" className={classes.genrePresence}>
              Select genre presence:
            </Typography>
            <Slider defaultValue={50} value={sliderValue} onChange={handleSliderChange} step={10} marks min={10} max={90} />
            <Grid container align="center">
              <Grid item xs={6}>
                {checkedCheckboxes[0].name}
              </Grid>
              <Grid item xs={6}>
                {checkedCheckboxes[1].name}
              </Grid>
              <Grid item xs={6}>
                <Typography variant="h3">{sliderValue}%</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="h3">{100 - sliderValue}%</Typography>
              </Grid>
            </Grid>
          </div>
        )}
      </FormControl>
    </Container>
  );
};

export default GenrePreference;
