import { Box, InputLabel, makeStyles, Select } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { playlistRequests } from '../../services/playlist-requests';
import Loading from '../Loading/Loading';

const useStyles = makeStyles({
  fullWidth: {
    width: '100%',
  },
});

const FilterByGenre = ({ filterByGenre }) => {

  const classes = useStyles();
  const [genreId, setGenreId] = useState(0);
  const [genres, setGenres] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    playlistRequests.getGenres(setGenres, setLoading);
  }, []);

  useEffect(() => {
    const genre = genres.find(g => g.dz_genre_id === genreId);
    filterByGenre(genre);
  }, [genreId]);

  return (
    <>
      {loading
        ? <Box><Loading /></Box>
        : <Box>
          <InputLabel>Filter by Genre</InputLabel>
          <Select className={classes.fullWidth}
            native
            value={genreId}
            onChange={(e) => setGenreId(+e.target.value)}
          >
            <option value={0} label="All">All</option>
            {genres.map(g => <option key={g.dz_genre_id} value={g.dz_genre_id} label={g.name}>{g.name}</option>)}
          </Select>
        </Box>
      }
    </>
  );

};

export default FilterByGenre;
