import {
  Button,
  ButtonGroup,
  Checkbox,
  Container,
  FormControlLabel,
  Grid,
  makeStyles,
  Paper,
  TextField,
  Typography,
} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import ReplayIcon from '@material-ui/icons/Replay';
import { useFormik } from 'formik';
import * as yup from 'yup';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import AuthContext from '../../providers/authContext.js';
import { saveNewPlaylistSchema } from '../../validations/save-new-playlist-schema.js';
import { playlistRequests } from '../../services/playlist-requests.js';
import Player from '../Player/Player.js';
import UserMessage from '../UserMessage/UserMessage';

const date = new Date();

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 400,
  },
  centered: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  hover: {
    '&:hover': {
      color: 'steelblue',
      cursor: 'pointer',
    },
  },
}));

const SaveNewPlaylist = ({ payload, locations, hidden, setHidden }) => {
  const classes = useStyles();
  const [newPlaylist, setNewPlaylist] = useState([]);
  const [changer, setChanger] = useState(0);
  const [image, setImage] = useState('noimage');
  const [imageChanger, setImageChanger] = useState(0);
  const auth = useContext(AuthContext);
  const schema = saveNewPlaylistSchema(yup);
  const history = useHistory();
  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: '',
  });

  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const formik = useFormik({
    initialValues: {
      name: `${locations.origin.toUpperCase()} to ${locations.destination.toUpperCase()} - ${date.toDateString().slice(4)}`,
    },
    validationSchema: schema,
    onSubmit: playlistRequests.saveNewPlaylist(newPlaylist, hidden, image, history, setUserMessage),
  });

  useEffect(() => {
    playlistRequests.generateNewPlaylist(payload, setNewPlaylist);
  }, [changer]);

  useEffect(() => {
    playlistRequests.setPlaylistPicture(setImage);
  }, [imageChanger]);

  const handleHiddenChange = (e) => setHidden(e.target.checked);

  return (
    <Container>
      <Paper>
        {newPlaylist.duration && <Player playlist={newPlaylist} />}
        {newPlaylist.message && (
          <Typography style={{ margin: '1em', color: 'red' }}>Not enough tracks for this trip. Regenerate or allow repeating artists.</Typography>
        )}
      </Paper>
      <form onSubmit={formik.handleSubmit} className={classes.form}>
        <Grid container spacing={3} justify="center" alignItems="center">
          <Grid item xs={9}>
            <TextField
              fullWidth
              required
              margin="normal"
              variant="outlined"
              id="name"
              name="name"
              label="New Playlist's Name"
              value={formik.values.name}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.name && Boolean(formik.errors.name)}
              helperText={formik.touched.name && formik.errors.name}
            />
          </Grid>
          <Grid item xs={3}>
            <FormControlLabel
              control={<Checkbox checked={hidden} onChange={handleHiddenChange} name="hidden-playlist-checkbox" />}
              label="Hidden playlist"
            />
          </Grid>
          <Grid item xs={6}>
            <Typography className={classes.hover} align="center" variant="subtitle2" onClick={() => setImageChanger((prev) => ++prev)}>
              Click for a new playlist cover suggestion
            </Typography>
            <img
              src={image}
              className={classes.hover}
              onClick={() => setImageChanger((prev) => ++prev)}
              style={{ borderRadius: '10px', margin: '1em' }}
            />
          </Grid>
        </Grid>
        <ButtonGroup className={classes.centered}>
          <Button
            color="primary"
            variant="contained"
            onClick={() => setChanger((prev) => ++prev)}
            className={classes.submit}
            startIcon={<ReplayIcon />}
          >
            Generate new
          </Button>
          <Button color="primary" variant="contained" type="submit" className={classes.submit} startIcon={<SaveIcon />}>
            Save Playlist
          </Button>
        </ButtonGroup>
      </form>
      <UserMessage
        open={userMessage.open}
        handleClose={userMsgClose}
        severity={userMessage.severity}
        message={<span>{userMessage.message}</span>} />
    </Container>
  );
};

export default SaveNewPlaylist;
