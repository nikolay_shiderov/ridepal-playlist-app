import { Avatar, Button, makeStyles, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import * as yup from 'yup';
import React from 'react';
import { CONSTANTS } from '../../common/constants';
import { updateAvatarSchema } from '../../validations/update-avatar-schema';


const useStyles = makeStyles({
  image: {
    width: '100%',
    height: '100%',
  },
  input: {
    display: 'none',
  },
});

const UserAvatar = ({ userData, updateAvatar }) => {

  const classes = useStyles();
  const schema = updateAvatarSchema(yup);

  const formik = useFormik({
    initialValues: {
      avatar: '',
    },
    validationSchema: schema,
  });

  const handleFileSelect = (e) => {
    formik.setFieldValue('avatar', e.target.files[0]);
  };

  const handleFileUpload = async () => {
    await updateAvatar(formik.values.avatar);
    formik.setFieldValue('avatar', '');
  };

  return (
    <>
      <Avatar
        className={classes.image}
        variant="rounded"
        src={`${CONSTANTS.API_URL}/uploads/avatars/${userData.avatar_url}`}
      />
      <input
        accept="image/*"
        className={classes.input}
        id="avatar"
        name="avatar"
        type="file"
        onChange={handleFileSelect}
      />
      <label htmlFor="avatar">
        <Button variant="outlined" component="span" fullWidth color="primary">
          Change Avatar
        </Button>
      </label>
      {formik.values.avatar
        ? (formik.errors.avatar)
          ? <Typography
            variant="body2"
            align="center"
            style={{ marginTop: '20px', color: 'red' }}>
            {formik.errors.avatar}
          </Typography>
          : <>
            <Typography
              variant="body2"
              align="center"
              style={{ marginTop: '20px' }}>
              {formik.values.avatar.name}
            </Typography>
            <Button
              variant="contained"
              fullWidth
              color="primary"
              onClick={handleFileUpload}>
              Upload file
            </Button>
          </>
        : null
      }
    </>
  );
};

export default UserAvatar;
