import { makeStyles, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow } from '@material-ui/core';
import React, { useState } from 'react';
import { CONSTANTS } from '../../common/constants';
import TrackSimple from '../TrackSimple/TrackSimple';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
});

const TrackList = ({ playlist, currentTrack, playTrack }) => {

  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };


  return (
    <div>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {CONSTANTS.TRACK_COLUMNS
                .map((key, index) =>
                  <TableCell
                    key={index}
                    style={{ fontWeight: 600, padding: '8px' }}
                  >
                    {key}
                  </TableCell>)}
            </TableRow>
          </TableHead>
          <TableBody>
            {playlist.tracks
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((track, index) => {
                return (
                  <TrackSimple
                    currentTrack={currentTrack}
                    key={track.dz_track_id}
                    track={track}
                    index={index}
                    page={page}
                    rowsPerPage={rowsPerPage}
                    playTrack={playTrack}
                  />
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      {playlist.tracks.length > 10 &&
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={playlist.tracks.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      }
    </div>
  );

};

export default TrackList;