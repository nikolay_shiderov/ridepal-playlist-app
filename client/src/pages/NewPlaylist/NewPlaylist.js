import { Container } from '@material-ui/core';
import React from 'react';
import Navbar from '../../components/Navbar/Navbar.js';
import Generator from '../../components/Generator/Generator.js';

const NewPlaylist = () => {
  return (
    <div>
      <Navbar />
      <Container maxWidth="md" style={{ marginTop: '4em' }}>
        <Generator />
      </Container>
    </div>
  );
};

export default NewPlaylist;
