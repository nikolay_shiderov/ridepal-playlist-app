import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { registerValidationSchema } from '../../validations/register-validation-schema.js';
import { Avatar, Button, Container, CssBaseline, TextField, Typography, makeStyles } from '@material-ui/core';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { onSubmitRegister } from '../../services/user-requests.js';
import UserMessage from '../../components/UserMessage/UserMessage';

const Register = () => {
  const history = useHistory();
  const schema = registerValidationSchema(yup);
  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: '',
  });

  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const formik = useFormik({
    initialValues: {
      username: '',
      email: '',
      password: '',
      passwordConfirmation: '',
    },
    validationSchema: schema,
    onSubmit: onSubmitRegister(history, setUserMessage),
  });

  const useStyles = makeStyles(theme => ({
    paper: {
      paddingTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <AssignmentIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form onSubmit={formik.handleSubmit} className={classes.form}>
          <TextField
            fullWidth
            required
            margin="normal"
            variant="outlined"
            id="username"
            name="username"
            label="Username"
            value={formik.values.username}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.username && Boolean(formik.errors.username)}
            helperText={formik.touched.username && formik.errors.username}
          />
          <TextField
            fullWidth
            required
            margin="normal"
            variant="outlined"
            id="email"
            name="email"
            label="Email"
            type="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <TextField
            fullWidth
            required
            margin="normal"
            variant="outlined"
            id="password"
            name="password"
            label="Password"
            type="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
          />
          <TextField
            fullWidth
            required
            margin="normal"
            variant="outlined"
            id="passwordConfirmation"
            name="passwordConfirmation"
            label="Confirm Password"
            type="password"
            value={formik.values.passwordConfirmation}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.passwordConfirmation && Boolean(formik.errors.passwordConfirmation)}
            helperText={formik.touched.passwordConfirmation && formik.errors.passwordConfirmation}
          />
          <Button color="primary" variant="contained" fullWidth type="submit" className={classes.submit}>
            Register
          </Button>
        </form>
      </div>
      <UserMessage
        open={userMessage.open}
        handleClose={userMsgClose}
        severity={userMessage.severity}
        message={<span>{userMessage.message}</span>} />
    </Container>
  );
};

export default Register;
