import { CONSTANTS } from '../common/constants.js';
import { getToken } from '../providers/authContext.js';

const deletePlaylistRequest = (playlistId, playlists, setPlaylists, setUserMessage) => {
  fetch(`${CONSTANTS.API_URL}/playlists/${playlistId}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.error) {
        throw new Error(data.error);

      } else {
        const updatedPlaylists = playlists.map((playlist) => (playlist.playlist_id === playlistId ? { ...playlist, is_deleted: 1 } : playlist));
        setUserMessage({
          open: true,
          severity: 'success',
          message: 'Playlist deleted!',
        });
        setTimeout(() => setPlaylists(updatedPlaylists), 1500);
      }
    })
    .catch(error => setUserMessage({
      open: true,
      severity: 'error',
      message: `${error.message}`,
    }));
};

const deleteUserRequest = (userId, users, setUsers, setUserMessage) => {
  fetch(`${CONSTANTS.API_URL}/admin/users/${userId}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.error) {
        throw new Error(data.error);
      } else {
        const updatedUsers = users.map((user) => (user.user_id === userId ? { ...user, is_deleted: 1 } : user));
        setUsers(updatedUsers);
        setUserMessage({
          open: true,
          severity: 'success',
          message: 'User deleted!',
        });
      }
    })
    .catch(error => setUserMessage({
      open: true,
      severity: 'error',
      message: `${error.message}`,
    }));
};

const fetchAllPlaylists = (setPlaylists) => {
  fetch(`${CONSTANTS.API_URL}/playlists/registered`, {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((response) => response.json())
    .then((data) => {
      setPlaylists(data);
    })
    .catch(console.warn);
};

const fetchAllUsers = (setUsers) => {
  fetch(`${CONSTANTS.API_URL}/admin/users`, {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((response) => response.json())
    .then((data) => {
      setUsers(data);
    })
    .catch(console.warn);
};

export const adminRequests = {
  deletePlaylistRequest,
  deleteUserRequest,
  fetchAllPlaylists,
  fetchAllUsers,
};
