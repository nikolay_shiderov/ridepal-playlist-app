export const registerValidationSchema = yup => {
  return yup.object({
    username: yup.string('Enter your username').min(4, 'Username should be of minimum 4 characters length').required('Username is required'),
    email: yup.string('Enter your email').email('Enter a valid email').required('Email is required'),
    password: yup.string('Enter your password').min(6, 'Password should be of minimum 6 characters length').required('Password is required'),
    passwordConfirmation: yup.string().oneOf([yup.ref('password'), null], 'Passwords must match'),
  });
};
