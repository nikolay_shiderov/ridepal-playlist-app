export const updateAvatarSchema = yup => {
  return yup.object({
    avatar: yup.mixed()
      .test('size', 'File size should be less than 1MB.', value => value === undefined || value.size <= 1000000)
      .test('type', 'Only image files are allowed.', value => value === undefined || (/\/(gif|jpe?g|png|webp|bmp)$/i).test(value.type)),
  });
};
