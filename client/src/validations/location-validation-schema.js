export const locationValidationSchema = (yup) => {
  return yup.object({
    origin: yup.string('Enter your origin location').min(4, 'Origin should be of minimum 4 characters length').required('Origin is required'),
    destination: yup
      .string('Enter your destination location')
      .min(4, 'Destination should be of minimum 4 characters length')
      .required('Destination is required'),
  });
};
